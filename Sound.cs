﻿using DxLibDLL;

namespace Shooting
{
    // 効果音管理クラス
    public static class Sound
    {
        // SE
        public static int bomb; // ボン！
        public static int Explosion;
        public static int PowerUP;
        public static int hit;


        // 効果音の読み込み
        public static void Load()
        {
            // 効果音の読み込み
            bomb = DX.LoadSoundMem("SE/bomb.wav");
            Explosion = DX.LoadSoundMem("SE/Explosion.wav");
            PowerUP = DX.LoadSoundMem("SE/Powerup.wav");
            hit = DX.LoadSoundMem("SE/Hit_Hurt.wav");
            // 効果音の音量変更
            ChangeVolumeSE(50.0f, bomb); // ボン！を５０％の音量で設定
            ChangeVolumeSE(50.0f, Explosion);

        }

        // 効果音を再生する。
        public static void PlaySE(int handle)
        {
            DX.PlaySoundMem(handle, DX.DX_PLAYTYPE_BACK);
        }

        // BGMを再生する。
        public static void PlayBGM(string fileName)
        {
            DX.PlayMusic(fileName, DX.DX_PLAYTYPE_LOOP); // 音楽の再生
        }

        // BGMの再生を停止する。
        public static void StopBGM()
        {
            DX.StopMusic();
        }

        // SE の音量を変更する（０～１００で指定する）
        public static void ChangeVolumeSE(float percent, int handle)
        {
            DX.ChangeVolumeSoundMem((int)(255.0f * percent / 100.0f), handle);
        }

        // BGM の音量を変更する（０～１００で指定する）
        public static void ChangeVolumeBGM(float percent)
        {
            DX.SetVolumeMusic((int)(255.0f * percent / 100.0f));
        }
    }
}
