﻿using DxLibDLL;
using MyLib;

namespace Shooting
{
    class ResultScene : Scene
    {
        public ResultScene(Game game)
            : base(game)
        {

        }

        public override void Init()
        {
        }

        public override void Update()
        {
            if (Input.GetButtonDown(DX.PAD_INPUT_1)) // Zキー（Aボタン）を押したら
            {
                game.ChangeScene(Game.State.TitleScene); // タイトルシーンに変更
            }
            else if (Input.GetButtonDown(DX.PAD_INPUT_2)) // Xキー（Bボタン）を押したら
            {
                game.ChangeScene(Game.State.GamePlayScene); // ゲームプレイシーンに変更
            }
        }

        public override void Draw()
        {
            DX.DrawString(0, 200, "Zキー（Aボタン）でタイトルに戻る", DX.GetColor(255, 255, 255));
            DX.DrawString(0, 230, "Xキー（Bボタン）で再スタート", DX.GetColor(255, 255, 255));
            DX.DrawGraphF(300, 100, Image.Result_image);
        }
    }
}
