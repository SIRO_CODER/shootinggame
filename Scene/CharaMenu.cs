﻿using DxLibDLL;
using MyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shooting
{
    class CharaMenu : Scene
    {
        public CharaMenu(Game game, Scene scene): base(game) 
        {
            this.scene = scene;
        }

        private Scene scene;
        public List<string> CharacterList = new List<string>();

        public override void Init()
        {
            CharacterList.Add("Debug Chara");
            CharacterList.Add("Balance");
        }

        private void CharaSet(string chara)
        {
            if (chara == "DebugChara")
            {
                player = new DebugChara(100, Screen.Height / 2, scene);
            }
            else if (chara == "Normal")
            {
                player = new NormalChara(100, Screen.Height / 2, scene);
            }
        }

        public override void Update()
        {
            if (Input.GetButtonDown(DX.PAD_INPUT_UP)) { }
            if (Input.GetButtonDown(DX.PAD_INPUT_DOWN)) { }
            if (Input.GetButtonDown(DX.PAD_INPUT_LEFT)) { }
            if (Input.GetButtonDown(DX.PAD_INPUT_RIGHT)) { }

            if(Input.GetButtonDown(DX.PAD_INPUT_1)) //決定
            {
                
            }

        }

        public void DrawCharaSet()
        {
            
        }

        public override void Draw()
        {

        }
    }
}
