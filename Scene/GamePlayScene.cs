﻿using DxLibDLL;
using MyLib;
using System.Collections.Generic;

namespace Shooting
{
    class GamePlayScene : Scene
    {
        public GamePlayScene(Game game)
            : base(game)
        {
        }

        public override void Init()
        {
            // 終了フラグを初期化
            isEnd = false;

            //自機を生成 
            player = new NormalChara(100, Screen.Height / 2, this);
            //自機弾のリストを生成
            playerBullets = new List<PlayerBullet>();
            enemies = new List<Enemy>();
            enemyBullets = new List<EnemyBullet>();
            explosions = new List<Explosion>();
            bossEffects = new List<Dia>();
            tpEffect = new List<Teleport>();
            //アイテムリストの生成
            items = new List<Item>();
            //マップの読み込み
            map = new Map(this, 0, "Map/stage1.csv");
            Sound.PlayBGM("BGM/BGM_Main.mp3");
            Sound.ChangeVolumeBGM(25);
            backGround = new BackGround();
        }

        public override void Update()
        {
            map.Scroll(scrollSpeed);
            backGround.Update();
            if (!player.isDead) player.Update(); // 自機の更新

            // 自機弾の更新処理
            foreach (PlayerBullet b in playerBullets)
            {
                b.Update();
            }

            foreach (EnemyBullet b in enemyBullets) b.Update();

            foreach (EnemyBullet enemyBullet in enemyBullets)
            {
                if (player.isDead) break;
                if (enemyBullet.isDead) continue;
                if (MyMath.CircleCircleIntersection(player.x, player.y, player.collision
                                                   , enemyBullet.x, enemyBullet.y, enemyBullet.collisionRadius))
                {
                    player.onCollisionEnemyBullet(enemyBullet);
                }
            }

            //敵の更新
            foreach (var e in enemies)
            {
                e.Update();
                e.EnemyOutBox(150);
            }

            foreach(Item item in items)
            {
                item.Update();
            }

            foreach (Item item in items)
            {
                if(MyMath.CircleCircleIntersection(player.x, player.y, player.Itemcollision, item.x, item.y, item.VisibleRadius))
                {
                    item.OnCollisionPlayer(player);
                    if (item.isDead) break;
                }
            }

            foreach (PlayerBullet bullet in playerBullets)
            {
                if (bullet.isDead) continue;

                foreach (Enemy e in enemies)
                {
                    if (e.isDead) continue;

                    if (MyMath.CircleCircleIntersection(bullet.x, bullet.y, bullet.collisionRadius,
                                                        e.x, e.y, e.CollisionRadius))
                    {
                        e.OnCollisionPlayerBullet(bullet);
                        bullet.OnCollisionEnemy(e);

                        if (bullet.isDead) break;
                    }

                }
            }

            foreach (Enemy e in enemies)
            {
                if (player.isDead) break;
                if (e.isDead) continue;

                if (MyMath.CircleCircleIntersection(player.x, player.y, player.collision, e.x, e.y, e.CollisionRadius))
                {
                    player.OnCollisionEnemy(e);
                }
            }

            //エフェクトの更新
            foreach (var explosion in explosions) explosion.Update();
            foreach (Dia bossEffect in bossEffects) bossEffect.Update();
            foreach (var tpEffect in tpEffect) tpEffect.Update();

            // リストから死んでるものを除去する
            playerBullets.RemoveAll(pb => pb.isDead);
            enemies.RemoveAll(e => e.isDead);
            enemyBullets.RemoveAll(eb => eb.isDead);
            explosions.RemoveAll(e => e.isDead);
            bossEffects.RemoveAll(e => e.isDead);
            tpEffect.RemoveAll(tp => tp.isDead);
            items.RemoveAll(i => i.isDead);

            if (isEnd || player.isDead) // 終了フラグが立ったら
            {
                game.ChangeScene(Game.State.ResultScene); // リザルトシーンに変更
            }
        }

        public override void Draw()
        {
            //背景の描画
            backGround.Draw();

            DX.DrawString(0, 0, "残基 : " + player.getLife() ,DX.GetColor(255, 255, 255));
            DX.DrawString(350, 0, "SCORE : " + player.getScore(), DX.GetColor(255, 255, 255));
            DX.DrawString(150, 0, "爆弾 : " + player.GetBombNum(), DX.GetColor(255, 255, 255));

            //ボスのエフェクト
            foreach (Dia bossEffect in bossEffects) bossEffect.Draw();

            //ボスのエフェクト
            foreach (var tpEffect in tpEffect) tpEffect.Draw();

            //敵の描画
            foreach (Enemy e in enemies) e.Draw();

            // 自機の描画
            if (!player.isDead) player.Draw(); 

            // 自機弾の描画
            foreach (PlayerBullet b in playerBullets) b.Draw();
            
            //敵弾の描画
            foreach (EnemyBullet b in enemyBullets) b.Draw();

            //アイテムの描画
            foreach (Item item in items) item.Draw();

            //爆発エフェクト
            foreach (Explosion ex in explosions) ex.Draw();


            //{//デバッグ情報
            //    DX.DrawString(0, 15, "敵の数 : " + enemies.Count, DX.GetColor(255, 255, 255));
            //    DX.DrawStringF(0, 30, "" + player.x + " : " + player.y, DX.GetColor(255, 255, 255));
            //    DX.DrawCircle((int)player.x, (int)player.y, (int)player.collision, DX.GetColor(255, 255, 255));
            //    foreach (PlayerBullet pb in playerBullets) DX.DrawCircle((int)pb.x, (int)pb.y, (int)pb.collisionRadius, DX.GetColor(255, 255, 255));
            //    //foreach (Item im in items) DX.DrawCircle((int)im.x, (int)im.y, (int)im.VisibleRadius, DX.GetColor(255, 255, 255));
            //}
        }
    }
}
