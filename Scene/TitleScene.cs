﻿using DxLibDLL;
using MyLib;

namespace Shooting
{
    class TitleScene : Scene
    {
        public TitleScene(Game game)
            : base(game)
        {
        }

        public override void Init()
        {
            Sound.PlayBGM("BGM/title.mp3"); // タイトルの音楽を鳴らす
            Sound.ChangeVolumeBGM(30.0f); // BGMの音量を 30 % にする
        }

        public override void Update()
        {
            if (Input.GetButtonDown(DX.PAD_INPUT_1)) // Z キー（Aボタン）を押したら
            {
                Sound.PlaySE(Sound.bomb); // ボンと鳴らす
                Sound.StopBGM(); // タイトルの音楽の再生を止める
                game.ChangeScene(Game.State.GamePlayScene); // ゲームプレイシーンに変更
            }
        }

        public override void Draw()
        {
            DX.DrawGraphF(60, 100, Image.Logo);
            DX.DrawString(0, 500, "ZキーまたはAボタンでゲームスタート", DX.GetColor(255, 255, 255));
        }
    }
}
