﻿using DxLibDLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shooting
{
    public class BackGround
    {
        float x0, x1;
        float y0, y1; 
        float velocity = 2;
        public void Update()
        {
            if (x0 > -Screen.Width) x0 -= velocity * 0.2f;
            else x0 = 0;
            if (x1 > -Screen.Width) x1 -= velocity * 1.02f;
            else x1 = 0;
        }
        public void Draw()
        {
            DX.DrawGraphF(x0, y0, Image.BackGround[0]);
            DX.DrawGraphF(x0 + Screen.Width, y0, Image.BackGround[0]);
            DX.DrawGraphF(x1, y1, Image.BackGround[2]);
            DX.DrawGraphF(x1 + Screen.Width, y1, Image.BackGround[2]);
        }
    }
}
