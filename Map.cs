﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shooting
{
    public class Map
    {
        const int Width = 1000;
        const int Height = 17;
        const int CellSize = 32;

        Scene scene;
        int[,] enemyData;
        float position;

        public Map(Scene scene, float startPosistion, string filePath)
        {
            this.scene = scene;
            position = startPosistion;
            Load(filePath);
        }

        private void Load(string filePath)
        {
            enemyData = new int[Width,Height];
            string[] lines = File.ReadAllLines(filePath);
            Debug.Assert(lines.Length == Height, "CSVファイルの高さが不正です:" + lines.Length);

            for(int y = 0; y < Height; y++)
            {
                string[] splitted = lines[y].Split(new Char[] {','});
                Debug.Assert(splitted.Length == Width, "CSVファイルの" + y + "行目が不正です:" + splitted.Length);

                for(int x = 0; x < Width; x++)
                {
                    enemyData[x, y] = int.Parse(splitted[x]);
                }
                
            }
        }

        public void Scroll(float delta)
        {
            int prevRightCell = (int)(position + Screen.Width) / CellSize;
            position += delta;
            
            int CurrentRightCell = (int)(position + Screen.Width) / CellSize;

            if(CurrentRightCell >= Width)
                return;
            
            if(prevRightCell == CurrentRightCell)
                return;

            float x = CurrentRightCell * CellSize - position;

            for(int cellY = 0; cellY < Height; cellY++)
            {
                float y = cellY * CellSize;
                int id = enemyData[CurrentRightCell, cellY];

                if(id == -1) continue;
                else if (id == 0) scene.enemies.Add(new Zako0(scene, x + 32, y + 32));
                else if (id == 1) scene.enemies.Add(new Zako1(scene, x + 32, y + 32));
                else if (id == 2) scene.enemies.Add(new Zako2(scene, x + 32, y + 32));
                else if (id == 3) scene.enemies.Add(new Zako3(scene, x + 32, y + 32));
                else if (id == 99) scene.enemies.Add(new ItemEnemy(scene, x + 32, y + 32));
                else if (id == 100) scene.enemies.Add(new Boss(scene, x + 90, y + 32));
                else Debug.Assert(false, "敵ID" + id + "番の生成は未実装です。");
            }
        }
    }
}
