﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shooting
{
    class NormalBullet : PlayerBullet
    {
        public NormalBullet(float x, float y, float angle, Scene scene) : base(x, y, angle, scene) { }

        public override void Update()
        {
            base.Update();
        }
    }
}
