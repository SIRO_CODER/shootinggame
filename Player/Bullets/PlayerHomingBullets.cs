﻿using DxLibDLL;
using MyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shooting
{
    class PlayerHomingBullets : PlayerBullet
    {
        public PlayerHomingBullets(float x, float y, float angle, Scene scene) : base(x, y, angle, scene) { }

        float distance = 0;

        public override void Update()
        {
            Enemy  e = NearestEnemy(scene.enemies);
            if (e != null)
            {
                angle = MyMath.PointToPointAngle(x, y, e.x, e.y);
            }

            base.Update();
        }

        public Enemy NearestEnemy(List<Enemy> EnemyList)
        {
            Enemy Nearest = null;
            foreach (Enemy e in EnemyList)
            {
                float preDistance = distance;
                distance = MyMath.Euclidean_distance(x, y, e.x, e.y);
                if (preDistance > distance && preDistance >= 1)
                {
                    Nearest = e;
                }
            }

            return Nearest;
        }
    }
}
