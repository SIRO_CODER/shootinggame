﻿using System; // Math使うのに必要
using DxLibDLL;

namespace Shooting
{
    // 自機弾クラス
    public abstract class PlayerBullet
    {   
        const int VisibleRadius = 16; // 見た目の半径

        public float Speed = 25f; // 移動速度
        public float x; // x座標
        public float y; // y座標
        public float collisionRadius = 16f;//衝突判定
        public bool isDead = false; // 死亡フラグ

        protected float vx; // x方向移動速度
        protected float vy; // y方向移動速度

        protected float angle; // 移動方向の角度
        protected Scene scene;
        protected Enemy enemy;
        
        // コンストラクタ
        public PlayerBullet(float x, float y, float angle, Scene scene)
        {
            this.x = x;
            this.y = y;
            this.scene = scene;
            this.angle = angle;
            vx = (float)Math.Cos(angle) * Speed;
            vy = (float)Math.Sin(angle) * Speed;
        }

        // 更新処理
        public virtual void Update()
        {
            MoveBullets();
            if (Offscreen()) isDead = true;
        }

        public void MoveBullets()
        {
            // 移動
            x += vx;
            y += vy;
        }

        // 描画処理
        public virtual void Draw()
        {
            DX.DrawRotaGraphF(x, y, 1, angle, Image.playerBullet);
        }

        public bool Offscreen()
        {
            // 画面外に出たら、死亡フラグを立てる
            if (x + VisibleRadius < 0 || x - VisibleRadius > Screen.Width ||
                y + VisibleRadius < 0 || y - VisibleRadius > Screen.Height)
            {
                return true;
            }

            return false;
        }
        // 敵にぶつかった時の処理
        public void OnCollisionEnemy(Enemy enemy)
        {
            isDead = true;
        }
    }
}

