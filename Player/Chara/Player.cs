﻿using DxLibDLL;
using MyLib;

namespace Shooting
{
    public abstract class Player
    {
        const float MoveSpeed = 6f; // 移動速度
        const int InvincibleTime = 50;

        public float x; // x座標
        public float y;// y座標

        public float BulletAngle;
        public float collision = 2f;
        public float Itemcollision = 40f;
        public bool isDead = false;
        public bool isScore = false;
        public long Score = 0;
        public int Power = 1;
        public int Bomb = 2;

        int life = 0;
        int invTimer = 0;
        protected int counter = 0;
        //rate of fire
        public int ShotDelay = 1;

        protected Scene scene; // Gameのインスタンスの参照

        // コンストラクタ
        // x : 初期位置x
        // y : 初期位置y
        // game : Gameのインスタンス
        public Player(float x, float y, Scene scene)
        {
            this.x = x;
            this.y = y;
            this.scene = scene;
        }
        // 更新処理
        public virtual void Update()
        {

            counter++;
            if (Input.GetButton(DX.PAD_INPUT_2)) MovingFunc(0.5f);
            else MovingFunc(1);

            ScreenOnCollision(ref x, ref y);

            // ボタン押下で自機弾を発射
            if (Input.GetButton(DX.PAD_INPUT_1)) Shot(ShotDelay);


            if (Input.GetButtonDown(DX.PAD_INPUT_3)) spShot();

            invTimer--;
        }

        private void MovingFunc(float VelocityDiameter)
        {
            float vx = 0f; // x方向移動速度
            float vy = 0f; // y方向移動速度

            if (Input.GetButton(DX.PAD_INPUT_LEFT))
            {
                vx = -MoveSpeed; // 左
            }
            else if (Input.GetButton(DX.PAD_INPUT_RIGHT))
            {
                vx = MoveSpeed; // 右
            }
            if (Input.GetButton(DX.PAD_INPUT_UP))
            {
                vy = -MoveSpeed; // 上
            }
            else if (Input.GetButton(DX.PAD_INPUT_DOWN))
            {
                vy = MoveSpeed; // 下
            }

            // 斜め移動も同じ速度になるように調整
            if (vx != 0 && vy != 0)
            {
                vx /= MyMath.Sqrt2;
                vy /= MyMath.Sqrt2;
            }


            // 実際に位置を動かす
            x += vx * VelocityDiameter;
            y += vy * VelocityDiameter;

        }

        private void ScreenOnCollision(ref float x, ref float y)
        {
            float playerX = x - collision;
            float playerY = y - collision;

            if (playerX < 0) x = 0;
            if (playerX >= Screen.Width - collision) x = Screen.Width - collision;
            if (playerY < 0) y = 0;
            if (playerY >= Screen.Height - collision) y = Screen.Height - collision;
        }

        public abstract void Shot(int ShotDelay);
        public void spShot()
        {
            if (Bomb <= 0) return;
            
            Bomb--;
            foreach(var bullets in scene.enemyBullets)
            {
                bullets.isDead = true;
                scene.items.Add(new ScoreUpItem(scene, bullets.x, bullets.y));
            }
        }

        // 描画処理
        public void Draw()
        {
            if (invTimer <= 0 || invTimer % 10 == 0)
                DX.DrawRotaGraphF(x, y, 1, 0, Image.player);
        }

        public void OnCollisionEnemy(Enemy enemy)
        {
            if (invTimer <= 0) TakeDamage();
        }

        public void TakeDamage()
        {
            life--;
            if (life <= 0)
            {
                isDead = true;
                scene.explosions.Add(new Explosion(x, y));
            }
            else invTimer = InvincibleTime;
        }

        public void onCollisionEnemyBullet(EnemyBullet enemyBullet)
        {
            if (invTimer <= 0) TakeDamage();
        }

        public int getLife()
        {
            return life;
        }

        public void AddLife(int Value)
        {
            life += Value;
        }

        public long getScore()
        {
            return Score;
        }

        public void AddScore(long Value)
        {
            Score += Value;
        }

        public int GetPower()
        {
            return Power;
        }
        public void AddPower()
        {
            Power+=10;
        }
        public int GetDelay()
        {
            return ShotDelay;
        }
        public void DonwDelay()
        {
            if(ShotDelay >= 2)
                ShotDelay--;
        }

        public void AddBomb()
        {
            Bomb++;
        }

        public int GetBombNum()
        {
            return Bomb;
        }
    }
}