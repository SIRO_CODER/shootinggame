﻿using MyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shooting
{
    class DebugChara : Player
    {
        public DebugChara(float x, float y, Scene scene) : base(x, y, scene) 
        {
            AddLife(50);
            ShotDelay = 2;
        }

        public override void Shot(int ShotDelay)
        {
            if(counter % ShotDelay == 0)
            {
                int way = Power;
                if (way > 5) way = 5;
                float Way_Angle = 6.0f;
                float startAngle = (way - 1) * Way_Angle / 2.0f;
                for (int i = 0; i < way; i++)
                {
                    //angle per shot
                    BulletAngle = (startAngle - Way_Angle * i) * MyMath.Deg2Rad;
                    scene.playerBullets.Add(new NormalBullet(x, y, BulletAngle, scene));
                }

                
            }
        }
    }
}
