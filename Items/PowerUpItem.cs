﻿using DxLibDLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shooting
{
    class PowerUpItem : Item
    {
        float velocity = 1.008f;
        float vx = 1.2f;
        Player player;
        public PowerUpItem(Scene scene, float x, float y) : base(scene, x, y) 
        {
            player = scene.player;
        }

        public override void Update()
        {

            
            vx *= velocity;
            x -= vx;


            base.Update();
        }

        public override void Draw()
        {
            DX.DrawRotaGraphF(x, y, 0.25, 0, Image.Item_Power);
        }

        public override void GetItem()
        {
            player.AddScore(1000);
            player.AddPower();
            player.DonwDelay();
            player.AddLife(1);
        }

        public bool offScrren()
        {
            // 画面外に出たら死亡フラグを立てる
            if (y + VisibleRadius < 0 || y - VisibleRadius > Screen.Height ||
                x + VisibleRadius < 0 || x - VisibleRadius > Screen.Width)
            {
                return true;
            }

            return false;
        }
    }
}
