﻿using DxLibDLL;
using MyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shooting
{
    class ScoreUpItem : Item
    {
        Player player;
        float vx;
        float vy;
        public ScoreUpItem(Scene scene, float x, float y) : base(scene, x, y)
        {
            player = scene.player;
        }
        public override void Update()
        {
            base.Update();

            float Angle = MyMath.PointToPointAngle(x, y, player.x, player.y);
            MoveSpeed(Angle, 3f);
            x += vx;
            y += vy;
        }
        public override void Draw()
        {
            DX.DrawRotaGraphF(x, y, 0.25, 0, Image.PointItem);
        }

        public override void GetItem()
        {
            player.AddScore(550);
        }

        public void MoveSpeed(float Angle, float Speed)
        {
            // 角度からx方向の移動速度を算出
            vx = (float)Math.Cos(Angle) * Speed;
            // 角度からy方向の移動速度を算出
            vy = (float)Math.Sin(Angle) * Speed;
        }
    }
}
