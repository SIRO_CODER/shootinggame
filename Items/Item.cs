﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shooting
{
    public abstract class Item
    {
        public readonly int VisibleRadius = 15;
        public bool isDead;
        public float x;
        public float y;
        
        protected Scene scene;

        public Item(Scene scene,float x, float y)
        {
            this.x = x;
            this.y = y;
            this.scene = scene;
        }

        public virtual void Update()
        {
            if (Offscreen()) isDead = true;
        }
        public abstract void Draw();
        public abstract void GetItem();

        public bool Offscreen()
        {
            // 画面外に出たら、死亡フラグを立てる
            if (x + VisibleRadius < 0 || x - VisibleRadius > Screen.Width ||
                y + VisibleRadius < 0 || y - VisibleRadius > Screen.Height)
            {
                return true;
            }

            return false;
        }

        public void OnCollisionPlayer(Player player)
        {
            GetItem();
            isDead = true;
        }

    }
}
