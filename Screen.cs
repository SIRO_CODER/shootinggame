﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shooting
{
    /// <summary>
    /// 画面解像度
    /// </summary>
    public static class Screen
    {
        /// <summary>
        /// 画面の幅 X
        /// </summary>
        public const int Width = 960;

        /// <summary>
        /// 画面の高さ Y
        /// </summary>
        public const int Height = 540;
    }
}
