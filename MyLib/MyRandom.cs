﻿using System;

namespace MyLib
{
    /// <summary>
    /// ランダム関連の便利クラス
    /// </summary>
    public static class MyRandom
    {
        // ゲーム中で唯一のインスタンス
        static Random random;

        /// <summary>
        /// 初期化（シード指定無し）
        /// </summary>
        public static void Init()
        {
            random = new Random();
        }

        // 
        /// <summary>
        /// 初期化（シードを指定）
        /// </summary>
        /// <param name="seed">シード</param>
        public static void Init(int seed)
        {
            random = new Random(seed);
        }

        /// <summary>
        /// 指定した範囲の整数の乱数を取得する（maxは出ないので注意）
        /// </summary>
        /// <param name="min">取得したい乱数の最小値</param>
        /// <param name="max">取得したい乱数の最大値</param>
        /// <returns>整数の乱数</returns>
        public static int Range(int min, int max)
        {
            return random.Next(min, max);
        }

        /// <summary>
        /// 指定した範囲の小数の乱数を取得する（maxは出ないので注意）
        /// </summary>
        /// <param name="min">取得したい乱数の最小値</param>
        /// <param name="max">取得したい乱数の最大値</param>
        /// <returns>小数の乱数</returns>
        public static float Range(float min, float max)
        {
            return (float)(random.NextDouble() * (max - min) + min);
        }

        /// <summary>
        /// 指定した確率（％）でtrueになる
        /// </summary>
        /// <param name="probability">指定する確率（％）</param>
        /// <returns>確率に従った結果（真理値）</returns>
        public static bool Percent(float probability)
        {
            return random.NextDouble() * 100 < probability;
        }

        /// <summary>
        /// 指定した範囲で乱数を返却する。
        /// 例えば1.5fを指定すると、-1.5～+1.5の範囲の値を返却する。
        /// </summary>
        /// <param name="value">指定する範囲</param>
        /// <returns>指定した範囲での乱数</returns>
        public static float PlusMinus(float value)
        {
            return Range(-value, value);
        }
    }
}
