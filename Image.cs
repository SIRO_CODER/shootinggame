﻿using DxLibDLL;

namespace Shooting
{
    // 画像管理クラス
    public static class Image
    {
        public static int player; // プレイヤー
        public static int playerBullet; // プレイヤーの弾
        public static int zako0; // ザコ0
        public static int zako1;
        public static int zako2;
        public static int zako3;
        public static int zako4;
        public static int enemyBullet16; // 敵弾
        public static int[] explosion = new int[16];
        public static int[] bossEffect = new int[30];
        public static int[] TpEffect = new int[30];
        public static int[] numberImage = new int[10];
        public static int[] BackGround = new int[4];
        public static int boss1; // ボス通常時
        public static int boss2; // ボス気絶
        public static int boss3; // ボス発狂
        public static int Item_Power;//パワーアップアイテム
        public static int PointItem;
        public static int Logo;
        public static int Result_image;


        public static void Load()
        {
            Logo = DX.LoadGraph("Image/logo.png");
            BackGround[0] = DX.LoadGraph("Image/BackGroundUnder.png");
            BackGround[1] = DX.LoadGraph("Image/BackGround01.png");
            BackGround[2] = DX.LoadGraph("Image/BackGround02.png");
            player = DX.LoadGraph("Image/player.png");
            playerBullet = DX.LoadGraph("Image/player_bullet.png");
            zako0 = DX.LoadGraph("Image/zako0.png");
            zako1 = DX.LoadGraph("Image/zako1.png");
            zako2 = DX.LoadGraph("Image/zako2.png");
            zako3 = DX.LoadGraph("Image/zako3.png");
            zako4 = DX.LoadGraph("Image/zako4.png");
            boss1 = DX.LoadGraph("Image/boss1.png");
            boss2 = DX.LoadGraph("Image/boss2.png");
            boss3 = DX.LoadGraph("Image/boss3.png");
            enemyBullet16 = DX.LoadGraph("Image/enemy_bullet_16.png");
            Item_Power = DX.LoadGraph("Image/powerItem.png");
            PointItem = DX.LoadGraph("Image/PointItem.png");
            Result_image = DX.LoadGraph("Image/Result.png");
            DX.LoadDivGraph("Image/explosion.png", 16, 8, 2, 64, 64, explosion);
            DX.LoadDivGraph("Image/Dia.png", 30, 6, 5, 100, 100, bossEffect);
            DX.LoadDivGraph("Image/Teleport.png", 30, 6, 5, 100, 100, TpEffect);

        }

        public static void DrawNum(float x, float y, uint num)
        {

        }
    }

}
