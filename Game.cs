﻿using System.Collections.Generic;
using DxLibDLL;
using MyLib;

namespace Shooting
{
    public class Game
    {
        // ゲーム状態種別
        public enum State
        {
            TitleScene, // タイトルシーン
            GamePlayScene, // ゲームプレイシーン
            ResultScene, // リザルトシーン
        }

        private List<Scene> scenes; // シーンのリスト
        private Scene currentScene; // 現在実行中のシーン
        public string Chara; //選択したキャラクター


        public void Init()
        {
            MyRandom.Init(); // ランダムを使う準備
            Input.Init(); // 入力を使う準備

            Image.Load();
            Sound.Load();

            // シーン生成
            scenes = new List<Scene>();
            scenes.Add(new TitleScene(this));
            scenes.Add(new GamePlayScene(this));
            scenes.Add(new ResultScene(this));

            ChangeScene(State.TitleScene); // タイトルシーンから開始
        }

        public void Update()
        {
            Input.Update(); // 入力の更新
            currentScene.Update();
        }

        public void Draw()
        {
            currentScene.Draw(); // 現在のシーンの描画
        }

        /// <summary>
        /// シーンを変更する
        /// </summary>
        /// <param name="next">次のシーンを表すゲーム状態種別</param>
        public void ChangeScene(State next)
        {
            currentScene = scenes[(int)next]; // シーン選択
            currentScene.Init(); // シーンの初期化
        }

        /// <summary>
        /// 指定されたシーンの描画を行う
        /// </summary>
        /// <param name="target">指定シーンを表すゲーム状態種別</param>
        public void DrawTargetScene(State target)
        {
            scenes[(int)target].Draw();
        }
    }
}
