﻿using DxLibDLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shooting
{
    public class Teleport
    {
        public float x;
        public float y;
        public bool isDead = false;

        int counter = 0;
        int imageIndex = 0;

        public Teleport(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        public void Update()
        {
            counter++;
            imageIndex = counter / 2;
            if (imageIndex >= 30) isDead = true;
        }

        public void Draw()
        {
            DX.DrawRotaGraphF(x, y, 2, 0, Image.TpEffect[imageIndex]);
        }
    }
}
