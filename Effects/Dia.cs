﻿using DxLibDLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shooting
{
    public class Dia
    {
        public float x;
        public float y;
        public bool isDead = false;

        int counter = 0;
        int imageIndex = 0;

        public Dia(float x, float y)
        {
            this.x = x;
            this.y = y;
        }

        public void Update()
        {
            counter++;
            imageIndex = counter / 3;
            if (imageIndex >= 30) isDead = true;
        }

        public void Draw()
        {
            DX.DrawRotaGraphF(x, y, 2, 0, Image.bossEffect[imageIndex]);
        }
    }
}
