﻿using DxLibDLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shooting
{
    class ItemEnemy : Enemy
    {
        public ItemEnemy(Scene scene, float x, float y) : base(scene, x, y) { }

        public override void Update()
        {
            x -= 3;
        }

        public override void Draw()
        {
            //DX.DrawRotaGraphF(x, y, 1, 1, 1);
        }

        public override void WhenDead()
        {
            scene.items.Add(new PowerUpItem(scene, x, y));
        }
    }
}
