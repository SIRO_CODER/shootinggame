﻿using DxLibDLL;
using MyLib;
using System;

namespace Shooting
{
    class Zako0 : Enemy
    {

        int counter = 0;
        bool isStoped = false;

        //float firing = 0;
        public Zako0(Scene scene, float x, float y) : base(scene, x, y)
        {
            life = 2;
        }

        public override void Update()
        {
            counter++;
            
            if(!isStoped)
            {
                x -= 3;
                if (x == Screen.Width - 200) isStoped = true;
            }
            if (counter % 10 == 0)
            {
                float firing = MyMath.PointToPointAngle(x, y, player.x, player.y);
                scene.enemyBullets.Add(new HomingBullet(scene, x, y, firing, 8f, 1.005f));
            }

            //if (counter % 2 == 0)
            //{
            //    firing = (180 * MyRandom.PlusMinus(10) * MyMath.Deg2Rad);
            //    scene.enemyBullets.Add(new EnemyBullet(x, y, firing, 8f));
            //}

            //if (counter % 1 == 0)
            //{
            //    firing = (float)(-Math.Atan2(x, y) * counter);

            //    scene.enemyBullets.Add(new EnemyBullet(x, y, firing, 8f));
            //    //scene.enemyBullets.Add(new EnemyBullet(x, y, firing + 180f * MyMath.Deg2Rad, 8f));
            //}

            //if (counter % 4 == 0)
            //{
            //    firing += 3f * MyMath.Deg2Rad;

            //    scene.enemyBullets.Add(new EnemyBullet(x, y, firing, 8f));
            //    scene.enemyBullets.Add(new EnemyBullet(x, y, firing + 180f * MyMath.Deg2Rad, 8f));
            //}

            //if (counter % 30 == 0)
            //{
            //    for (float angle = 0; angle < 360; angle += 10)
            //    {
            //        firing = angle * MyMath.Deg2Rad;
            //        scene.enemyBullets.Add(new EnemyBullet(x, y, firing, 8f));
            //    }
            //}

            //if (counter % 10 == 0)
            //{
            //    float angle = MyMath.PointToPointAngle(x, y, scene.player.x, scene.player.y);
            //    int way = 8;
            //    float Way_Angle = 4.0f;
            //    float startAngle = (way - 1) * Way_Angle / 2.0f;
            //    for (int i = 0; i < way; i++)
            //    {
            //        //angle per shot
            //        float aps = (startAngle - Way_Angle * i) * MyMath.Deg2Rad;
            //        GenBullet(x, y, angle+aps, 8f);
            //    }
            //}

        }
        public override void Draw()
        {
            DX.DrawRotaGraphF(x, y, 1, 0, Image.zako0);
        }
    }
}
