﻿using System; // 三角関数を使うのに必要
using DxLibDLL;
using MyLib; // MyMath を使うのに必要

namespace Shooting
{
    // ザコ2クラス
    public class Zako2 : Enemy
    {
        // コンストラクタ
        public Zako2(Scene scene, float x, float y)
            : base(scene, x, y)
        {
            life = 500;
        }
        int counter;
        float angle;
        // 更新処理
        public override void Update()
        {
            counter++;
            if (750 < x) x -= 5;
            else if(counter % 3 == 0)
            {
                angle += -3f * MyMath.Deg2Rad;
                uint nway = 12;
                for(int i = 1; i <= nway; i++)
                {
                    float DeltaAngle = (360 / nway) * i * MyMath.Deg2Rad;

                    scene.enemyBullets.Add(new SingleBullets(scene, x, y, angle + DeltaAngle, 3.0f, 1.005f));//右

                    scene.enemyBullets.Add(new SingleBullets(scene, x, y, -angle + DeltaAngle, 3.0f, 1.005f));//左
                }
                
            }
        }

        // 描画処理
        public override void Draw()
        {
            DX.DrawRotaGraphF(x, y, 1, 0, Image.zako2);
        }
    }
}