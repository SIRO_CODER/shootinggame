﻿using DxLibDLL;
using MyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Shooting
{
    public abstract class Enemy
    {
        public float x;
        public float y;
        public float CollisionRadius = 32f;
        public bool isDead = false;
        protected float life = 1;
        protected long Score = 1000;

        protected Scene scene;
        protected Player player;

        public Enemy(Scene scene, float x, float y)
        {
            this.scene = scene;
            player = scene.player;
            this.x = x;
            this.y = y;
        }

        public abstract void Update();

        public abstract void Draw();

        public virtual void WhenDead()
        {
            
        }

        public void EnemyOutBox(int ExtendedRange)
        {
            if (x < -ExtendedRange || y > Screen.Height + ExtendedRange) isDead = true;
            return;
        }
        // 自機弾に当たったときの処理
        public virtual void OnCollisionPlayerBullet(PlayerBullet playerBullet)
        {
            life -= 1;
            if (life <= 0)
            {
                isDead = true;
                WhenDead();
                player.AddScore(Score);
                Sound.PlaySE(Sound.Explosion);
                scene.explosions.Add(new Explosion(x, y));
            }
        }
    }
}
