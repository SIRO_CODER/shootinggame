﻿using System; // 三角関数を使うのに必要
using DxLibDLL;
using MyLib; // MyMath を使うのに必要

namespace Shooting
{
    // ザコ4クラス
    public class Zako4 : Enemy
    {
        // コンストラクタ
        public Zako4(Scene scene, float x, float y)
            : base(scene, x, y)
        {
            // ライフ設定
            life = 1;
        }

        // 更新処理
        public override void Update()
        {
            // とりあえず左へ移動
            x -= 1f;
        }

        // 描画処理
        public override void Draw()
        {
            DX.DrawRotaGraphF(x, y, 1, 0, Image.zako4);
        }
    }
}