﻿using DxLibDLL;
using MyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shooting
{
    class Zako1 : Enemy
    {
        int conuter;
        public Zako1(Scene scene, float x, float y) : base(scene, x, y) 
        {
            life = 10;
        }

        public override void Update()
        {
            conuter++;
            if (x > 750) x-=3;
            else
            {
                if (conuter % 40 == 0)
                {
                    for (float angle = 0; angle < 360f; angle += 10f)
                    {
                        float firingAngle = angle * MyMath.Deg2Rad;
                        scene.enemyBullets.Add(new SingleBullets(scene, x, y, firingAngle, 8f, 1.003f));
                    }
                }
            }
        }

        public override void WhenDead()
        {
            int BulletCount = MyRandom.Range(5, 10);
            for (int i = 0; i <= BulletCount; i++)
            {
                scene.enemyBullets.Add(new HomingBullet(scene, MyRandom.PlusMinus(100) + x, MyRandom.PlusMinus(100) + y, 1, 8f, 1.008f));
            }
        }

        public override void Draw()
        {
            DX.DrawRotaGraphF(x, y, 1, 0, Image.zako1);
        }

    }
}
