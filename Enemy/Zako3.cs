﻿using System; // 三角関数を使うのに必要
using DxLibDLL;
using MyLib; // MyMath を使うのに必要

namespace Shooting
{
    // ザコ3クラス
    public class Zako3 : Enemy
    {
        int counter;
        // コンストラクタ
        public Zako3(Scene scene, float x, float y)
            : base(scene, x, y)
        {
            // ライフ設定
            life = 5;
        }

        // 更新処理
        public override void Update()
        {
            counter++;
            // とりあえず左へ移動
            if (counter % 60 == 0)
            {
                x = MyRandom.Range(Screen.Width / 2, Screen.Width - 20);
                y = MyRandom.Range(20, Screen.Height);
                scene.tpEffect.Add(new Teleport(x, y));
            }

            if(counter % 60 <= 10)
            {
                scene.enemyBullets.Add(new HomingBullet(scene, x, y, 1, 3.002f, 1.02f));
            }
        }

        public override void WhenDead()
        {
            int BulletCount = MyRandom.Range(5, 10);
            for (int i = 0; i <= BulletCount; i++)
            {
                scene.enemyBullets.Add(new HomingBullet(scene, MyRandom.PlusMinus(100) + x, MyRandom.PlusMinus(100) + y, 1, 8f, 1.008f));
            }
        }

        // 描画処理
        public override void Draw()
        {
            DX.DrawRotaGraphF(x, y, 1, 0, Image.zako3);
        }
    }
}