﻿using DxLibDLL;
using MyLib;
using System;


namespace Shooting
{
    public abstract class EnemyBullet
    {
        const float VisibleRadius = 8;

        public float x; // x座標
        public float y; // y座標
        public bool isDead = false; // 死亡フラグ
        public float collisionRadius = 8f; // 当たり判定半径

        protected float Angle;
        protected float Speed;
        protected Scene scene;
        protected Player player;

        float vx; // x方向移動速度
        float vy; // y方向移動速度
        float velocity;// 加速度

        public EnemyBullet(Scene scene, float x, float y, float Angle, float Speed, float velocity) 
        {
            this.scene = scene;
            player = scene.player;
            this.x = x;
            this.y = y;
            this.Angle = Angle;
            this.Speed = Speed;
            //加速度の絶対値を計算
            this.velocity = Math.Abs(velocity);
            MoveSpeed(Angle, Speed);
        }

        public EnemyBullet(Scene scene, float x, float y, float Angle, float Speed)
        {
            this.scene = scene;
            player = scene.player;
            this.x = x;
            this.y = y;
            this.Angle = Angle;
            this.Speed = Speed;
            velocity = 1;
            MoveSpeed(Angle, Speed);
        }

        public virtual void Update()
        {
            MoveSpeed(Angle, Speed);
            Speed *= velocity;
            x += vx;
            y += vy;
            if (offScrren()) isDead = true;
        }

        public void MoveSpeed(float Angle, float Speed)
        {
            // 角度からx方向の移動速度を算出
            vx = (float)Math.Cos(Angle) * Speed;
            // 角度からy方向の移動速度を算出
            vy = (float)Math.Sin(Angle) * Speed;
        }

        public bool offScrren()
        {
            // 画面外に出たら死亡フラグを立てる
            if (y + VisibleRadius < 0 || y - VisibleRadius > Screen.Height ||
                x + VisibleRadius < 0 || x - VisibleRadius > Screen.Width)
            {
                return true;
            }

            return false;
        }

        // 描画処理
        public void Draw()
        {
            DX.DrawRotaGraphF(x, y, 1, (float)-Math.Atan2(x, y), Image.enemyBullet16);
        }
    }
}
