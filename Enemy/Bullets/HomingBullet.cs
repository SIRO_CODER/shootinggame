﻿using MyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shooting
{
    class HomingBullet : EnemyBullet
    {
        public HomingBullet(Scene scene, float x, float y, float angle, float Speed, float velocity) : base(scene, x, y, angle, Speed, velocity) { }
        public HomingBullet(Scene scene, float x, float y, float angle, float Speed) : base(scene, x, y, angle, Speed) { }

        int HomingTime = 20;
        public override void Update()
        { 
            HomingTime--;
            if(HomingTime > 0)
                Angle = MyMath.PointToPointAngle(x, y, player.x, player.y);

            base.Update();
        }
    }
}
