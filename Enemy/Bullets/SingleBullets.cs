﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shooting
{
    class SingleBullets : EnemyBullet
    {
        public SingleBullets(Scene scene, float x, float y, float angle, float Speed, float velocity) : base(scene, x, y, angle, Speed, velocity) { }
        public SingleBullets(Scene scene, float x, float y, float angle, float Speed) : base(scene, x, y, angle, Speed) { }

        public override void Update()
        {
            base.Update();
        }
    }
}
