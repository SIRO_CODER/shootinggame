﻿using DxLibDLL;
using MyLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shooting
{
    public class Boss : Enemy
    {
        enum State
        {
            Appear,
            Normal,
            Swoon,
            Angry,
            Dying
        }

        State state = State.Appear;
        int NormalTime = 0;//ノーマルモード
        int swoonTime = 120;//気絶
        int restartTime = 40;
        int angryTime = 0;//発狂
        int dyingTime = 180;//死亡
        
        float angle = 0;
        float centerX;
        float centerY;
        float rpX;
        float rpY;

        float vx;
        float vy;


        public Boss(Scene scene, float x, float y) : base(scene, x, y) 
        {
            life = 1500;
            CollisionRadius = 70;
            Score = 10000;
        }
        public override void Update()
        {
            if (state == State.Appear)
            {//登場
                x -= 1;
                if (x <= 750) 
                {
                    state = State.Normal;

                    rpX = x;
                    rpY = y;

                    vx = 0;
                    vy = 2f;

                }
            }
            else if (state == State.Normal)
            {//通常
                NormalTime++;
                //vx vy 分だけ移動
                x += vx;
                y += vy;

                if(y <= 50)
                {
                    y = 50;//y座標を50にフィット
                    vy = -vy;//縦方向の速度を反転
                }
                else if(y >= 500)
                {
                    y = 500;//座標を500にフィット
                    vy = -vy;//縦方向の速度を反転
                }
                if(NormalTime % 20 == 0)
                {
                    for(int i = 0; i < 360; i+=10)
                    {
                        float angle = i;
                        scene.enemyBullets.Add(new SingleBullets(scene, x, y, angle, 8f, 1.005f));
                    }
                }

                if(NormalTime % 120 == 0)
                {
                    x = MyRandom.Range(0, Screen.Width);
                    y = MyRandom.Range(10, Screen.Height);
                    scene.tpEffect.Add(new Teleport(x, y));
                }
            }
            else if (state == State.Swoon)
            {//発狂へ移行
                swoonTime--;
                if(swoonTime % 30 == 0) scene.bossEffects.Add(new Dia(x, y));
                if (swoonTime > restartTime)
                {

                    const float vDamp = 0.995f;

                    //元の位置に戻る
                    x += vx;
                    y += vy;

                    //減衰
                    vx *= vDamp;
                    vy *= vDamp;
                }
                else
                {
                    const float Agility = 0.07f;
                    x = x + (rpX - x) * Agility;
                    y = y + (rpY - y) * Agility;
                }
                if(swoonTime <= 0)
                {
                    state = State.Angry;
                    x = rpX;
                    y = rpY;

                    centerX = x;
                    centerY = y;
                }
            }
            else if(state == State.Angry)
            {//発狂状態の処理
                //突進する
                angryTime++;//インクリメント
                if(angryTime % 30 == 0) scene.bossEffects.Add(new Dia(x, y));

                //突進の動き
                //角度(調整) * 角度 * rad変換 
                //float rmTheta = 1.0f * angryTime * MyMath.Deg2Rad;
                //x = centerX + 300f * ((float)Math.Cos(2f * rmTheta) -1f);
                //y = centerY + 150f * (float)Math.Sin(rmTheta);

                if (angryTime % 8 == 0)
                {
                    uint Way = 6;
                    for (int i = 0; i < Way; i++)
                    {
                        float vec = (360 / Way) * i;
                        float angle = (float)-Math.Atan2(x, y) * angryTime + vec;
                        scene.enemyBullets.Add(new SingleBullets(scene, x, y, angle * MyMath.Deg2Rad, 1.1f));
                        scene.enemyBullets.Add(new SingleBullets(scene, x, y, -angle * MyMath.Deg2Rad, 1.1f));
                    }
                }

            }
            else if(state == State.Dying)
            {//死亡時の処理
                dyingTime--;

                y += 2f;
                angle += 0.05f * MyMath.Deg2Rad;

                float vi_theta = 20f * dyingTime * MyMath.Deg2Rad;
                x = centerX + 3f *(float)Math.Cos(5 * vi_theta);
                y = centerY + 3f * (float)Math.Sin(5 * vi_theta);

                Action explode = () 
                    => scene.explosions.Add(new Explosion(x + MyRandom.PlusMinus(100), y + MyRandom.PlusMinus(100)));

                if (dyingTime % 5 == 0)
                {
                    explode();
                }

                if (dyingTime <= 0)
                {
                    for (int i = 0; i < 20; i++)
                    {
                        explode();
                    }

                    isDead = true;
                    scene.isEnd = true;
                }

            }
        }
        public override void WhenDead()
        {

        }
        public override void Draw()
        {
            if(state == State.Appear || state == State.Normal)
            {
                DX.DrawRotaGraphF(x, y, 1, 0, Image.boss1);
            }
            else if(state == State.Swoon)
            {
                DX.DrawRotaGraphF(x, y, 1, 0, Image.boss2);
            }
            else if(state == State.Angry)
            {
                DX.DrawRotaGraphF(x, y, 1, 0, Image.boss3);
            }
            else if(state == State.Dying)
            {
                DX.DrawRotaGraphF(x, y, 1, angle, Image.boss2);
            }
        }

        public override void OnCollisionPlayerBullet(PlayerBullet playerBullet)
        {
            if (state == State.Appear || state == State.Swoon || state == State.Dying)
                return;
            
            life--;

            if(life <= 0)
            {
                state = State.Dying;
                centerX = x;
                centerY = y;

            }
            else if(state == State.Normal && life <= 750)
            {
                state = State.Swoon;
                //気絶時の速度（初速）
                vx = 1.2f;
                vy = 1.2f;
            }
        }
    }
}
