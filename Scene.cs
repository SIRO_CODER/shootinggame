﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shooting
{
    /// <summary>
    /// シーンの抽象クラス。
    /// </summary>
    public abstract class Scene
    {
        /// <summary>
        /// Game クラスのインスタンスの参照
        /// </summary>
        protected Game game;

        /// <summary>
        ///  シーンの終了フラグ
        /// </summary>
        public bool isEnd = false;

        // フィールドの追加　ここから
        public Player player; // 自機
        public List<PlayerBullet> playerBullets; // 自機弾のリスト
        public List<Enemy> enemies; // 敵のリスト
        public List<EnemyBullet> enemyBullets; // 敵弾のリスト
        public List<Explosion> explosions; // 爆発エフェクトのリスト
        public List<Dia> bossEffects; //ボスエフェクト
        public List<Teleport> tpEffect;
        public float scrollSpeed = 1.5f; // スクロール速度
        public List<Item> items;//アイテムリスト

        public Map map; // マップ
        public BackGround backGround;

        // フィールドの追加　ここまで

        /// <summary>
        /// コンストラクタ
        /// </summary>
        /// <param name="game">Game クラスのインスタンスの参照</param>
        public Scene(Game game)
        {
            this.game = game;
        }

        /// <summary>
        /// シーンの初期化（シーンが切り替わるときに、最初に１度だけ呼ばれる）
        /// </summary>
        public abstract void Init();

        /// <summary>
        /// シーンの更新
        /// </summary>
        public abstract void Update();

        /// <summary>
        /// シーンの描画
        /// </summary>
        public abstract void Draw();
    }
}
